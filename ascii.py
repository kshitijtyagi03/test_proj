# Define a function to return the uppercase version of a letter
def makeUppercase(char):
    if 'a' <= char <= 'z':
        return chr(ord(char) - ord('a') + ord('A'))
    return char

# Define a function to return the lowercase version of a letter
def makeLowercase(char):
    if 'A' <= char <= 'Z':
        return chr(ord(char) - ord('A') + ord('a'))
    return char

# Define a function that returns True if the letter is an alphabet
def isAlphabet(char):
    return 'a' <= char <= 'z' or 'A' <= char <= 'Z'

# Define a function that returns True if an element is a digit
def isDigit(char):
    return '0' <= char <= '9'

# Define a function to determine if the given character is a special character or not
def isSpecialChar(char):
    special_chars = "!@#$%^&*()-_=+[]{}|;:'\",.<>?/\\"
    return char in special_chars

