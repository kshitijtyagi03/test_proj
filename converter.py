def convert_bytes(bytes_value):
    kilobytes = bytes_value / 1000
    kibibytes = bytes_value / 1024
    megabytes = kilobytes / 1000
    mebibytes = kibibytes / 1024
    return kilobytes, kibibytes, megabytes, mebibytes

def main():
    while True:
        user_input = input("Enter the number of bytes (enter 0 to quit): ")

        try:
            bytes_value = int(user_input)
            if bytes_value == 0:
                print("Goodbye!")
                break  # Exit the loop if the user enters 0
            elif bytes_value < 0:
                print("Please enter a positive number of bytes.")
            else:
                kilobytes, kibibytes, megabytes, mebibytes = convert_bytes(bytes_value)
                print(f"{bytes_value} bytes is equal to:")
                print(f"{kilobytes:.2f} kilobytes (KB)")
                print(f"{kibibytes:.2f} kibibytes (kiB)")
                print(f"{megabytes:.2f} megabytes (MB)")
                print(f"{mebibytes:.2f} mebibytes (MiB)")
        except ValueError:
            print("Invalid input. Please enter a positive integer number of bytes.")

if __name__ == "__main__":
    main()
